# Sum

Все нижеприведенные команды выполняются из директории проекта, 
если не сказано обратное.

### Установка:

`pip install -r requirements.txt`

### Переменные окружения
`LOG_LEVEL`
`DATABASE_TYPE`
`DATABASE_USER`
`DATABASE_PASS`
`DATABASE_HOST`
`DATABASE_PORT`
`DATABASE_NAME`
`WORKERS`


### Запуск сервера

`uvicorn app.primary_adapters.api:app --reload --host 0.0.0.0 --port 8000`

### Тестирование

Запуск тестов:

`pytest app tests`
