from multiprocessing import Pool
from typing import Any

from fastapi import FastAPI, Depends
from pydantic import BaseModel
from sqlalchemy import create_engine

from app.core.use_cases import SaveModuloData, GetModuloData
from app.settings import DATABASE_URL, settings
from app.secondary_adapters.database.db import Database
from app.secondary_adapters.database.repo import Repo
from app.secondary_adapters.database.tables import metadata

worker = Pool(settings.WORKERS)
engine = create_engine(DATABASE_URL)

metadata.create_all(engine)

db = Database(engine=engine)
repo = Repo(db=db)

app = FastAPI()


def get_save_modulo_data() -> SaveModuloData:
    return SaveModuloData(repo=repo, worker=worker)


def get_modulo_data() -> GetModuloData:
    return GetModuloData(repo=repo)


class Params(BaseModel):
    js: Any


@app.post("/data")
def save_modulo_data_request(params: Params, save_sum_data: SaveModuloData = Depends(get_save_modulo_data)):
    save_sum_data.save(params.js)


@app.get("/data")
def get_modulo_data_request(sum_value: int, get_sum_data: GetModuloData = Depends(get_modulo_data)):
    return get_sum_data.get(sum_value)
