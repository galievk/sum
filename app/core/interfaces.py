from abc import ABC, abstractmethod
from typing import List

from .entities import ModuloData


class IRepo(ABC):

    @abstractmethod
    def add(self, modulo_data: ModuloData) -> None: ...

    @abstractmethod
    def get_modulo_data_by_sum(self, sum_value: int) -> List: ...
