from collections.abc import Iterable
from multiprocessing import Pool
from typing import Any

from app.core.entities import ModuloData
from app.core.interfaces import IRepo


def int_generator(js: Any):
    if isinstance(js, dict):
        js = js.values()
    for el in js:
        if isinstance(el, Iterable) and not isinstance(el, (str, bytes)):
            yield from int_generator(el)
        elif type(el) is int:
            yield el


def get_modulo(js: Any) -> int:
    sum_value = 0
    if isinstance(js, Iterable) and not isinstance(js, (str, bytes)):
        for item in int_generator(js):
            sum_value += item
    elif isinstance(js, int):
        sum_value = js
    modulo = sum_value % 65536
    return modulo


class SaveModuloData:
    def __init__(self, repo: IRepo, worker: Pool):
        self.repo = repo
        self.worker = worker

    def save(self, js: Any):
        sum_value = self.worker.apply(get_modulo, (js,))
        self.repo.add(ModuloData(sum_value, js))


class GetModuloData:
    def __init__(self, repo: IRepo):
        self.repo = repo

    def get(self, sum_value: int):
        modulo_data_list = self.repo.get_modulo_data_by_sum(sum_value)
        modulo_data_list = [{"js": sum_data.data} for sum_data in modulo_data_list]
        return modulo_data_list
