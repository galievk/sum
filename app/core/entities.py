from dataclasses import dataclass
from typing import Any


@dataclass
class ModuloData:
    modulo: int
    data: Any

