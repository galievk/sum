from typing import Optional

from pydantic import BaseSettings


class Settings(BaseSettings):
    LOG_LEVEL: str = "DEBUG"
    DATABASE_TYPE: str = 'mysql+pymysql'
    DATABASE_USER: str
    DATABASE_PASS: str
    DATABASE_HOST: str
    DATABASE_PORT: Optional[str]
    DATABASE_NAME: str
    WORKERS: int = 5


settings = Settings()


DATABASE_URL = \
    f'{settings.DATABASE_TYPE}://' \
    f'{settings.DATABASE_USER}:' \
    f'{settings.DATABASE_PASS}@' \
    f'{settings.DATABASE_HOST}' \
    f'{":" + settings.DATABASE_PORT if settings.DATABASE_PORT else ""}' \
    f'/{settings.DATABASE_NAME}'
