from typing import List

from sqlalchemy import select
from sqlalchemy.orm import Session

from app.core.interfaces import IRepo
from .db import Database
from ...core.entities import ModuloData


class Repo(IRepo):

    def __init__(self, db: Database):
        self.db = db

    @property
    def session(self) -> Session:
        return self.db.get_session()

    def add(self, modulo_data: ModuloData) -> None:
        self.session.add(modulo_data)
        self.session.commit()
        self.session.close()

    def get_modulo_data_by_sum(self, sum_value: int) -> List:
        result = self.session.execute(select(ModuloData).where(ModuloData.modulo == sum_value)).scalars().all()
        self.session.close()
        return result
