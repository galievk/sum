import threading

from sqlalchemy.engine import Engine
from sqlalchemy.orm import Session


class Database:
    def __init__(self, engine: Engine):
        self.engine = engine
        self._storage = threading.local()

    def get_session_if_exists(self):
        return getattr(self._storage, 'session', None)

    def get_session(self) -> Session:
        session = self.get_session_if_exists()
        if session is None:
            session = Session(self.engine)
            self._storage.session = session
        return session
