from sqlalchemy import Table, Column, Integer, MetaData, JSON

metadata = MetaData()

modulo_data = Table(
    'sum_data', metadata,
    Column('id', Integer, primary_key=True),
    Column('modulo', Integer, nullable=False),
    Column('data', JSON, nullable=False)
)
