from sqlalchemy.orm import registry

from app.core.entities import *

from .tables import *

mapper_registry = registry()

mapper_registry.map_imperatively(ModuloData, modulo_data)
