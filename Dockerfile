FROM python:3.9.5-slim-buster
RUN mkdir /code
WORKDIR /code

ADD requirements.txt /code/
RUN pip3 install -r requirements.txt

ADD app /code/app

CMD uvicorn app.primary_adapters.api:app --reload --host 0.0.0.0 --port 8000