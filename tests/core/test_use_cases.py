from copy import deepcopy
from multiprocessing import Pool
from unittest.mock import Mock

import pytest

from app.core.entities import ModuloData
from app.core.interfaces import IRepo
from app.core.use_cases import GetModuloData, SaveModuloData, get_modulo, int_generator


def test_int_generator():
    items = []
    for item in int_generator({"z": [1, "a", 0.5, 1], "": True}):
        items.append(item)

    assert items == [1, 1]


def test_get_modulo():
    modulo = get_modulo([2])
    assert modulo == 2

    modulo = get_modulo({"z": [1, "a", 0.5, 1], "": True})

    assert modulo == 2

    modulo = get_modulo("2")

    assert modulo == 0

    modulo = get_modulo(2)

    assert modulo == 2


class CopyingMock(Mock):
    def __call__(self, *args, **kwargs):
        self.args = deepcopy(args)
        self.kwargs = deepcopy(kwargs)
        return super().__call__(*args, **kwargs)


@pytest.fixture
def repo():
    repo = Mock(IRepo)

    def add(modulo_data):
        pass

    repo.add = CopyingMock(side_effect=add)
    repo.get_modulo_data_by_sum = Mock(
        return_value=[ModuloData(2, [2]), ModuloData(2, {"z": [1, "a", 0.5, 1], "": True})])
    return repo


def test_get_sum_data(repo):
    get_sum_data = GetModuloData(repo=repo)

    result = get_sum_data.get(2)

    assert result == [{"js": [2]}, {"js": {"z": [1, "a", 0.5, 1], "": True}}]


def test_save_sum_data(repo):
    save_modulo_data = SaveModuloData(repo=repo, worker=Pool(1))

    save_modulo_data.save([2])
    assert repo.add.args[0] == ModuloData(2, [2])

    save_modulo_data.save({"z": [1, "a", 0.5, 1], "": True})
    assert repo.add.args[0] == ModuloData(2, {"z": [1, "a", 0.5, 1], "": True})

    save_modulo_data.save(2)
    assert repo.add.args[0] == ModuloData(2, 2)

    save_modulo_data.save("2")
    assert repo.add.args[0] == ModuloData(0, "2")
