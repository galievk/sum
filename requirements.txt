fastapi==0.66.1
uvicorn==0.14.0
SQLAlchemy==1.4.21
PyMySQL==1.0.2
pytest==6.2.4